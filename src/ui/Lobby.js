/**
 * Created by lixiaodong on 16/4/7.
 */
var LobbyUI = cc.Layer.extend({

    backGround      :   null,
    helpButton      :   null,
    settingButton   :   null,
    background      :   null,
    infoButton      :   null,
    shopButton      :   null,
    baseDir         :   '',
    tableTap        :   null,
    tableTapMenu    :   null,
    tableTapSprite  :   null,
    table_sb        :   null,
    beginSprite     :   null,
    notifyMenu      :   null,
    table_element   :   null,
    taskMenu        :   null,
    size            :   0,
    infoMenu        :   null,
    rankingMenu     :   null,
    friendsMenu     :   null,
    shopMenu        :   null,

    ctor: function () {
        this._super();
        this.baseDir = "#img/index/";
        this.size = cc.director.getWinSize();

        this.initBg();
        this.initTable();
    },
    initBg: function () {
        var size = cc.director.getWinSize();
        var bg_default = new cc.Sprite(res.bg_lobby_default);
        var bg_light   = new cc.Sprite(res.bg_lobby_light);

        bg_default.x = size.width / 2;
        bg_default.y = size.height / 2;

        bg_default.setOpacity(2);
        var fadeOutAction = new cc.FadeOut(0);
        bg_default.runAction(fadeOutAction);
        this.addChild(bg_default);

        bg_light.x = size.width / 2;
        bg_light.y = size.height / 2;

        bg_light.setOpacity(0);

        var fadeInAction = new cc.FadeIn(2);
        bg_light.runAction(fadeInAction);

        this.background = bg_light;

        this.addChild(bg_light,0);
    },

    initTable: function () {
        this.initShopMenu();

        this.initTableTapMenu();
        this.initTableTapSprite();
        this.initHaloAnimation();

        this.initTableSB();
        this.initBeganSprite();
        this.initBeganAnimation();

        this.initNotifyMenu();
        this.initTableElement();
        this.initTaskMenu();

        this.initHelpButton();
        this.initSettingButton();

        this.initLobbyPageView();

        this.initImageScrollView();

        this.initRankingMenu();
        this.initFriendsMenu();
        this.initUserInfoMenu();

    },
    initTableTapMenu: function () {
        var table_tap = new cc.MenuItemImage(this.baseDir+'table-tap.png',this.baseDir+'table-tap.png',this.onStartGame);
        table_tap.x = this.size.width / 2;
        table_tap.y = this.size.height / 2 + 120;
        this.tableTap = table_tap;
        var table_tap_menu = new cc.Menu(this.tableTap);
        table_tap_menu.setPosition(cc.p(0,0));
        this.tableTapMenu = table_tap_menu;
        this.addChild(this.tableTapMenu);
    },
    initTableTapSprite: function () {
        var table_tap_sprite = new cc.Sprite(this.baseDir+'table-tap.png');
        table_tap_sprite.x = this.size.width / 2;
        table_tap_sprite.y = this.size.height / 2 + 120;
        this.tableTapSprite = table_tap_sprite;
        this.addChild(this.tableTapSprite,0);
    },
    initNotifyMenu: function () {
        var notifyMenuItem = new cc.MenuItemImage(this.baseDir+'notify2.png',this.baseDir+'notify2.png',this.onNotify);
        notifyMenuItem.x = this.tableTap.x - 130;
        notifyMenuItem.y = this.tableTap.y - 50;

        var notifyMenu = new cc.Menu(notifyMenuItem);
        notifyMenu.setPosition(cc.p(0,0));
        this.notifyMenu = notifyMenu;
        this.addChild(this.notifyMenu);
    },
    initTableElement: function () {
        var table_element = new cc.Sprite(this.baseDir+'table-element.png');
        table_element.x = this.tableTap.x;
        table_element.y = this.tableTap.y - 50;
        this.table_element = table_element;
        this.addChild(this.table_element);
    },
    initTaskMenu: function () {
        var taskIcon = new cc.MenuItemImage(this.baseDir+'icon-envelop.png',this.baseDir+'icon-envelop.png',this.onTask);
        taskIcon.x = this.tableTap.x + 130;
        taskIcon.y = this.tableTap.y - 50;

        var taskMenu = new cc.Menu(taskIcon);
        taskMenu.setPosition(cc.p(0,0));
        this.taskMenu = taskMenu;
        this.addChild(this.taskMenu);
    },
    initBeganSprite:   function () {
        var beginSprite = new cc.Sprite(this.baseDir+'text-start.png');
        beginSprite.x = this.tableTap.x;
        beginSprite.y = this.tableTap.y + 40;
        this.beginSprite = beginSprite;
        this.addChild(this.beginSprite);
    },
    initBeganAnimation: function () {
        //快速开始动画
        var animFrames = [];
        for(var i = 0 ; i < 13; i ++){
            var frameName;
            if(i < 9){
                frameName = "img/index/quick_began00"+(i+1)+".png";
            } else {
                frameName = "img/index/quick_began0"+(i+1)+".png";
            }
            var spriteFrame = cc.spriteFrameCache.getSpriteFrame(frameName);
            animFrames.push(spriteFrame);
        }

        var animation = new cc.Animation(animFrames,0.25);
        var action = new cc.Animate(animation);
        this.beginSprite.runAction(cc.repeatForever(action));
    },
    initHaloAnimation: function () {
        //光环动画
        var animFrames = [];
        for(var i = 0 ; i < 3; i ++){
            var frameName = "img/index/start-"+(i+1)+".png";
            var spriteFrame = cc.spriteFrameCache.getSpriteFrame(frameName);
            animFrames.push(spriteFrame);
        }
        var animation= new cc.Animation(animFrames,1);
        var action = new cc.Animate(animation);
        this.tableTapSprite.runAction(cc.repeatForever(action));
    },
    initTableSB: function () {
        var table_sb = new cc.Sprite(this.baseDir+'table-sb.png');
        table_sb.x = this.tableTap.x;
        table_sb.y = this.tableTap.y + 120;
        this.table_sb = table_sb;
        this.addChild(table_sb,2);
    },
    initHelpButton: function () {
        var helpItem = new cc.MenuItemImage(this.baseDir+'btn-help.png',this.baseDir+'btn-help.png',this.onHelp);
        helpItem.x = 50;
        helpItem.y = this.size.height - 150;
        var helpMenu = new cc.Menu(helpItem);
        helpMenu.setPosition(cc.p(0,0));
        this.helpButton = helpMenu;
        this.addChild(this.helpButton);
    },
    initSettingButton: function () {
        var settingItem = new cc.MenuItemImage(this.baseDir+'btn-setting.png',this.baseDir+'btn-help.png',this.onSetting);
        settingItem.x = this.size.width - 50;
        settingItem.y = this.size.height - 150;
        var settingMenu = new cc.Menu(settingItem);
        settingMenu.setPosition(cc.p(0,0));
        this.settingButton = settingMenu;
        this.addChild(this.settingButton);
    },
    initShopMenu: function () {
        var shopItem = new cc.MenuItemImage(this.baseDir+'shop.png',this.baseDir+'shop.png',this.onShop);
        shopItem.x = this.size.width / 2;
        shopItem.y = this.size.height - 150;
        var shopMenu = new cc.Menu(shopItem);
        shopMenu.setPosition(cc.p(0,0));
        this.shopButton = shopMenu;
        this.addChild(this.shopButton);
        //shop-light.png 闪烁
    },
    initRankingMenu: function () {
        //leaderboard.png
        var rankingItem = new cc.MenuItemImage(this.baseDir+'leaderboard.png',this.baseDir+'leaderboard.png',this.onRanking);
        rankingItem.x = this.size.width - 50;
        rankingItem.y = 160;
        var rankingMenu = new cc.Menu(rankingItem);
        rankingMenu.setPosition(cc.p(0,0));
        this.rankingMenu = rankingMenu;
        this.addChild(this.rankingMenu);
    },
    initFriendsMenu: function () {
        //btn-friends.png
        var friendsItem = new cc.MenuItemImage(this.baseDir+'btn-friends.png',this.baseDir+'btn-friends.png',this.onFriends);
        friendsItem.x = 50;
        friendsItem.y = 160;
        var friends = new cc.Menu(friendsItem);
        friends.setPosition(cc.p(0,0));
        this.friendsMenu = friends;
        this.addChild(this.friendsMenu);
    },
    initLobbyPageView: function () {
        
    },
    initUserInfoMenu: function () {
        //组合 layer
    },
    onFriends: function () {
        
    },
    onRanking: function () {

    },
    onStartGame: function () {
        cc.log('onStartGame');
        //todo 获取房间玩家信息
        cc.director.runScene(new GameScene({params:'hello world!'}));
    },
    onHelp: function () {
        cc.log('help-->>');
    },
    onSetting: function () {
        cc.log('setting-->>');
    },
    onTask: function () {
        cc.log('onTask-->>');

        //cc.director.runScene(new cc.TransitionRotoZoom(new TaskScene()))
    },
    onNotify: function () {
        cc.log('onNotify-->>');
    },
    onShop: function () {

    },
    initImageScrollView: function () {
        var pageView = new ccui.PageView();

        pageView.setTouchEnabled(true);
        pageView.setContentSize(cc.size(240,900));
        pageView.setAnchorPoint(cc.p(0.5,0.5));
        pageView.x = this.size.width / 2;
        pageView.y = this.size.height / 2 - 300;

        for(var i = 0 ; i < 4; i++){
            var layout = new ccui.Layout();

            var imageView = new ccui.ImageView();

            imageView.loadTexture('img/index/activity.png',ccui.Widget.PLIST_TEXTURE);
            imageView.x = pageView.width / 2;
            imageView.y = pageView.height / 2;

            layout.addChild(imageView);

            var text = new ccui.Text();
            text.string = "Page " + (i+1);
            text.font = "30px 'Marker Felt'";

            layout.addChild(text);

            pageView.addPage(layout);
        }

        pageView.addEventListener(this.pageViewEvent,this);

        this.addChild(pageView);
    },
    pageViewEvent:function(sender,type){
        switch(type){
            case ccui.PageView.EVENT_TURNING:
                cc.log("Page: "+sender.getCurPageIndex());
                break;
        }
    },
    initShop: function () {

    },
    initUserInfo: function () {

    }

});