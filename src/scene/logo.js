/**
 * Created by lixiaodong on 16/4/1.
 */

var LogoLayer = cc.Layer.extend({
    ctor: function () {
        this._super();
        var size = cc.director.getWinSize();

        var sprite = new cc.Sprite(res.app_name);
        sprite.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        this.addChild(sprite, 0);

        sprite.setOpacity(0); //0 - > 2

        var action = new cc.FadeIn(2);

        sprite.runAction(action);

        return true;
    }
});

var LogoScene = cc.Scene.extend({
    onEnter: function () {
        this._super();

        var layer = new LogoLayer();

        this.addChild(layer);

        console.log('LogoScene-=-=>>');


        setTimeout(function () {
            var loginScene = new LoginScene();
            cc.director.runScene(new cc.TransitionFade(3.0,loginScene));
        },2000);
    }
});